#!/bin/sh

#set -e

echo "Got merge request $CI_COMMIT_REF_NAME for branch $CI_PROJECT_NAME!!"

echo "PROJECTID: $CI_MERGE_REQUEST_PROJECT_ID"
echo "MERGE REQUEST ID:$CI_MERGE_REQUEST_IID"
# echo "MR_TOKEN:$MR_TOKEN"

# Analyze code
sl analyze --version-id "$CI_COMMIT_SHA" --tag branch="$CI_COMMIT_REF_NAME" --app "$CI_PROJECT_NAME" --wait target/hello-shiftleft-0.0.1.jar

COMMENT_BODY='{"body":""}'
COMMENT_BODY=$(echo "$COMMENT_BODY" | jq '.body += "<img height=20 src=\"https://www.shiftleft.io/static/images/ShiftLeft_logo_white.svg\"/> — NG SAST Analysis Findings\n===\n\n"')


SCANID=$(curl -H "Authorization: Bearer $SHIFTLEFT_API_TOKEN" "https://www.shiftleft.io/api/v4/orgs/$SHIFTLEFT_ORG_ID/apps/$CI_PROJECT_NAME/findings" | jq -c -r '.response.scan.id')

echo "SCANID: $SCANID"

SUMMARY=$(curl -H "Authorization: Bearer $SHIFTLEFT_API_TOKEN" "https://www.shiftleft.io/api/v4/orgs/$SHIFTLEFT_ORG_ID/apps/$CI_PROJECT_NAME/scans/$SCANID" | jq -r -c '.response.counts[] | select(.key == "language") | "* \(.count) \(.severity)"')

echo "SUMMARY: $SUMMARY"

NEW_FINDINGS=$(curl -H "Authorization: Bearer $SHIFTLEFT_API_TOKEN" "https://www.shiftleft.io/api/v4/orgs/$SHIFTLEFT_ORG_ID/apps/$CI_PROJECT_NAME/scans/compare?source=tag.branch=master&target=tag.branch=$CI_COMMIT_REF_NAME" | jq -c -r '.response.new | .? | .[] | "* [ID " + .id + "](https://www.shiftleft.io/findingDetail/" + .app + "/" + .id + "): " + "["+.severity+"] " + .title')

COMMENT_BODY=$(echo "$COMMENT_BODY" | jq ".body += \"### Vulnerability Summary\n\n\"")
COMMENT_BODY=$(echo "$COMMENT_BODY" | jq ".body += \"$SUMMARY\n\n\"")
COMMENT_BODY=$(echo "$COMMENT_BODY" | jq ".body += \"### New findings\n\n\"")
COMMENT_BODY=$(echo "$COMMENT_BODY" | jq ".body += \"$NEW_FINDINGS\n\n\"")

echo "COMMENT_BODY:$COMMENT_BODY"

curl -s -XPOST "https://gitlab.com/api/v4/projects/$CI_MERGE_REQUEST_PROJECT_ID/merge_requests/$CI_MERGE_REQUEST_IID/notes" \
  -H "PRIVATE-TOKEN: $MR_TOKEN" \
  -H "Content-Type: application/json" \
  -d "$COMMENT_BODY"

sl check-analysis --app "$CI_PROJECT_NAME" --source 'tag.branch=master' --target "tag.branch=$CI_COMMIT_REF_NAME"